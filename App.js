import React from 'react';
import { FlatList } from 'react-native';
import { StyleSheet, View, ImageBackground } from 'react-native';
import ForecastCard from './components/ForecastCard';
import TempNow from './components/TempNow';
import GoogleAutoComplete from './components/GoogleAutoComplete'

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      latitude: 0,
      longitude: 0,
      forecast: [],
      current: [],
      temperature: 0,
      description: ' ',
      icon: ' ',
      error: ' ',
    }
  }

  componentDidMount() {
    this.getLocation();
  }

  getLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState(
          (prevState) => ({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }), () => { this.getWeather(); }
        );
      },
      (error) => this.setState({ forecast: error.message }),
      { enableHighAccuracy: true, timeout: 2000, maximumAge: 1000 }
    );
  }

  // From GoogleAutoComplete
  useNewLocation = (newLat, newLong) => {
    console.log(newLat, newLong)
    this.setState({
        latitude : newLat,
        longitude : newLong
    })
    this.getWeather()
  }

  getWeather() {
    let url = 'https://api.openweathermap.org/data/2.5/onecall?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=b9b5ce6dc941edff964bd3a68a1af92e';
    fetch(url)
    .then(response => response.json())
    .then(data => {
      this.setState((prevState, props) => ({
        forecast: data,
        current: data.current,
        temperature: data.current.temp,
        description: data.current.weather[0].description,
        icon: data.current.weather[0].icon,
      }))
      console.log(data)
    })
    .catch(error => console.log(error));
  }

  render() {
    return (
      <View style = {styles.container}>
        <ImageBackground source = {require('./Photos/Summer.jpeg')} style = {{width: '100%', height: '100%'}}>
        <GoogleAutoComplete useNewLocation = {this.useNewLocation.bind(this)}/>
        <TempNow
          temperature = {this.state.temperature}
          description = {this.state.description}
          icon = {this.state.icon}
          data = {this.state.current}
          location = {this.state.forecast}
        />
        {/*
        <View style = {styles.container}>
          <Image style = {styles.image} source = {{uri:"https://openweathermap.org/img/w/" + this.state.icon + ".png"}}/>
          <Text>{this.state.description}</Text>
          <Text>Today's Temperature is {Math.round(this.state.temperature * 10) / 10}&#8451;</Text>
        </View>*/}

        <FlatList data = {this.state.forecast.daily}
          style = {styles.container}
          keyExtractor = {item => item.dt} // Forces list to use ids for keys rather than default "key" prop
          renderItem = {({item}) => // Tells React to render list with Forecast cards
          <ForecastCard detail = {item} />}
        />
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    width: 100,
    height: 100,
  },
});

/*
import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import TInput from './components/textinput'

export default function App() {

  return (
    <View style = {styles.container}>
      <Text style = {styles.text}>Weather</Text>
      <TInput />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center', 
  },
  text: {
    color: 'orange'
  },
})

import React from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

export default class App extends React.Component {

  state = {
    text: '',
    todos: []
  }

  _handlePress = () => {
    let { todos, text } = this.state
    todos.push(text)

    this.setState({text: '', todos})
  }
  _removeTodo = (index) => {
    let {todos} = this.state
    todos.splice(index, 1)
    this.setState({todos})
  }
  _renderTodos = () => {
    let col = "#ddd"
    let {todos} = this.state
    return todos.map((ele, i) => {
      i % 2 == 0 ? col = '#ddd' : col = 'transparent'
    return (
      <View key = {i} 
        style = {{
          flexDirection: 'row', 
          justifyContent: 'space-around', 
          marginTop: 5, 
          minWidth: 300,
          backgroundColor: col,
          borderColor: 'black',
          borderWidth: 1
        }}>
        <Text style = {{padding: 12}}>{ele}</Text>
        <Button color = "red" title = 'x' onPress = {() => this._removeTodo(i)}></Button>
      </View>
    )
    })
  }

  render() {
    return (
      <View style = {styles.container}>
        
      <Text style = {{fontSize: 30}}>{this.state.text}</Text>
      <TextInput
        style = {styles.input}
        onChangeText = {(text) => this.setState({text})}
        value = {this.state.text}
      />
      <View style = {styles.button}>
        <Button title = "press" onPress = {this._handlePress}/>
      </View>
      {this._renderTodos()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 30, 
  },
  button: {
    marginTop: 10,
    borderRadius: 5,
  },
  input: {
    height: 40,
    borderColor: 'black',
    width: 110,
    borderWidth: 1
  },
});

import React from 'react';
import { FlatList } from 'react-native';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import ForecastCard from './components/ForecastCard';
import TempNow from './components/TempNow';
import GoogleAutoComplete from './components/GoogleAutoComplete'

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      latitude: 0,
      longitude: 0,
      forecast: [],
      temperature: 0,
      error: ' ',
    }
  }

  componentDidMount() {
    this.getLocation();
  }

  getLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState(
          (prevState) => ({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }), () => { this.getWeather(); }
        );
      },
      (error) => this.setState({ forecast: error.message }),
      { enableHighAccuracy: true, timeout: 2000, maximumAge: 1000 }
    );
  }

  useNewLocation = (newLat, newLong) => {
    console.log(newLat, newLong)
    this.setState({
        latitude : newLat,
        longitude : newLong
    })
    this.getWeather()
}

  getWeather() {
    let url = 'https://api.openweathermap.org/data/2.5/forecast?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=b9b5ce6dc941edff964bd3a68a1af92e';
    fetch(url)
    .then(response => response.json())
    .then(data => {
      this.setState((prevState, props) => ({
        forecast: data,
        temperature: data.list[0].main.temp_max
      }))
      console.log(data)
    })
    .catch(error => console.log(error));
  }

  render() {
    return (
      <View>
        <GoogleAutoComplete useNewLocation = {this.useNewLocation.bind(this)}/>
        <TempNow />
       
        <Text>Today's Temperature is {this.state.temperature}</Text>
        
        <FlatList data = {this.state.forecast.list}
          style = {styles.container}
          keyExtractor = {item => item.dt_text} // Forces list to use ids for keys rather than default "key" prop
          renderItem = {({item}) => // Tells React to render list with Forecast cards
          <ForecastCard detail = {item} location = {this.state.forecast.city.name} />}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20, 
  },
});
*/