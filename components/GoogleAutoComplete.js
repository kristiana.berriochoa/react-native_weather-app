import React from 'react';
import { StyleSheet, View } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

export default class GoogleAutoComplete extends React.Component {

   newLocation(details) {
        console.log("New Location:", details);   
        let newLat = details.geometry.location.lat;
        let newLong = details.geometry.location.lng; 
        this.props.useNewLocation(newLat, newLong)
    }

    render() {
        return (
            <View style = {styles.container}>
                <GooglePlacesAutocomplete
                    placeholder = "Search"
                    minLength = {1} // Minimum length of text to search
                    autoFocus = {false}
                    returnKeyType = {'search'}  // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                    listViewDisplayed = 'auto' // True/false/undefined, 'auto'
                    fetchDetails = {true}
                    onPress = {(data, details = null) => {
                        this.newLocation(details, details.geometry.location.lat, details.geometry.location.lng);
                        return(null);
                    }}
                    getDefaultValue = {() => {
                        return ''; // Text input default value
                    }}
                    query = {{ // Available options: https://developers.google.com/places/web-service/autocomplete
                        key: 'AIzaSyBkFH6kJKpaDy_n2GBxHfPFlvdW38Oo-24',
                        language: 'en', // Language of the results
                        types: '(cities)', // Default: 'geocode'
                    }}
                    style = {{
                    }}
                    currentLocation = {false} // Will add a 'Current location' button at the top of the predefined places list
                    currentLocationLabel = "Current location"
                    debounce = {300}  // Debounce request time in ms (default: 300)
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        //flex: 1,
        flexDirection: 'row',
        paddingTop: 30,
        paddingBottom: 0,
    },
})