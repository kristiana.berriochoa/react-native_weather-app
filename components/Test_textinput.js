import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Text, Button } from 'react-native';

export default function App() {
  const [email, setEmail] = useState('')
  let submit = () => {setEmail('')}
  return (
      <View>
      <TextInput
      style = {styles.input}
        onChangeText = {(text) => setEmail(text)}
        value = {email}
        onSubmitEditing = {(e) => setEmail(e.nativeEvent.text)}
      /> 
      <Button onPress = {submit} title = 'send' color = 'pink'></Button>
      <Text>From the input: {email}</Text>
      </View>
  );
}

const styles = StyleSheet.create({
  input: {
    height: 60,
    borderColor: 'black',
    width: 100,
    borderWidth: 1
  }
});

/*
import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Text, Button } from 'react-native';

export default function App() {
  const [email, setEmail] = useState('')
  let submit = () => {setEmail('')}
  return (
      <View>
      <TextInput
      style = {styles.input}
        onChangeText = {(text) => setEmail(text)}
        value = {email}
        onSubmitEditing = {(e) => setEmail(e.nativeEvent.text)}
      /> 
      <Button onPress = {submit} title = 'send' color = 'pink'></Button>
      <Text>From the input: {email}</Text>
      </View>
  );
}

const styles = StyleSheet.create({
  input: {
    height: 60,
    borderColor: 'black',
    width: 100,
    borderWidth: 1
  }
});
*/