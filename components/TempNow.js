import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Text } from 'react-native-elements';

const TempNow = (props) => {

  //let time; // Format date
  let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var date = new Date(props.data.dt * 1000); // Create a new date from passed date time
   //var hours = date.getHours(); // Hours from timestamp
    //var minutes = "0" + date.getMinutes(); // Minutes from timestamp
    //let dayName = days[date.getDay()]; // Weekday name
    let day = date.getDate(); // Numerical date
    let year = date.getFullYear();
    let month = months[date.getMonth()]; // Month name
    //time = hours + ':' + minutes.substr(-2);


  return( 
    <View style = {styles.card}>
      
      <Text style = {styles.title}>{props.location.timezone}</Text>
      <View style = {styles.container}>
          <Image style = {styles.image} source = {{uri:"https://openweathermap.org/img/w/" + props.icon + ".png"}}/>
        <View style = {styles.innerContainer}>
          <Text style = {styles.number}>{Math.round(props.temperature * 10) / 10}&#8451;</Text>
          <Text style = {styles.notes}>{props.description}</Text>
        </View>
      </View> 
        <Text style = {styles.today}>Currently</Text>
        <Text style = {styles.today}>{month} {day}, {year}</Text>
    </View>
  );
}

export default TempNow;

const styles = StyleSheet.create({
  card: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    textAlign: 'center',
    height: 300,
    borderColor: '#FAF0E6',
    borderWidth: 1,
  },  
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  innerContainer: {
    padding: 25,
  },
  today: {
    fontSize: 21,
    fontWeight: 'bold',
    color: '#F0F8FF',
    textAlign: 'center'
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#F0F8FF',
    textAlign: 'center',
    marginTop: 15,
  },
  number: {
    fontSize: 50,
    color: '#CD5C5C',
    fontWeight: 'bold',
    textDecorationStyle: 'solid',
  },
  notes: {
    fontSize: 20,
    fontWeight: 'bold',
    textTransform: 'capitalize',
    color: '#CD5C5C',
  },
  image: {
    width: 150,
    height: 150,
},
})