import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Text } from 'react-native-elements';
//import moment from 'moment';

export default class ForecastCard extends React.Component {

    state = { }

    componentDidMount() {
        let time; // Format date
        let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var date = new Date(this.props.detail.dt * 1000); // Create a new date from passed date time
            var hours = date.getHours(); // Hours from timestamp
            var minutes = "0" + date.getMinutes(); // Minutes from timestamp
            let dayName = days[date.getDay()]; // Weekday name
            let day = date.getDate(); // Numerical date
            time = hours + ':' + minutes.substr(-2);
        this.setState({
            dayName,
            day,
            time
        })
    }

    render() {
        return(
            <View style = {styles.card}>
                <View style = {styles.container}>
                    <View style = {styles.innerContainer}>
                        <Text style = {styles.date}>{this.state.dayName}</Text>
                    </View>

                    <Image style = {styles.image} source = {{uri:"https://openweathermap.org/img/w/" + this.props.detail.weather[0].icon + ".png"}}/>
                    
                    <View style = {styles.innerContainer}>
                        <Text style = {styles.numberDay}>{Math.round(this.props.detail.temp.day * 10) / 10}&#8451;</Text>
                        <Text style = {styles.numberNight}>/ {Math.round(this.props.detail.temp.night * 10) / 10}&#8451;</Text>
                        <Text style = {styles.notes}>{this.props.detail.weather[0].description}</Text>
                    </View>
                </View>  
            </View>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: 'transparent',
        borderColor: '#FAF0E6',
        borderWidth: 1,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    date: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#008B8B',
        paddingLeft: 20,
    },
    innerContainer: {
        flexDirection: 'column',
        textAlign: 'left',
        width: '30%',
    },
    numberDay: {
        fontSize: 25,
        color: '#008B8B',
        fontWeight: 'bold',
        paddingLeft: 15,
    },
    numberNight: {
        color: '#008B8B',
        fontSize: 15,
        paddingLeft: 15,
    },
    notes: {
        fontSize: 15,
        textTransform: 'capitalize',
        color: '#008B8B',
        fontWeight: 'bold',
        textAlign: 'left',
        paddingLeft: 15,
    },
    image: {
        width: 100,
        height: 100,
    },
});

/*
dateBuilder = (d) => {
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    let day = days[d.getDay()];
    let date = d.getDate();
    let month = months[d.getMonth()];
    let year = d.getFullYear();

    return `${day} ${date} ${month}, ${year}`
}

var i = 0;
var data = { list: [ { dt: 1522666800 } ] };

var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']; 
var dayNum = new Date(data.list[i].dt * 1000).getDay();
var result = days[dayNum];
console.log(result);

import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Text, Card, Divider } from 'react-native-elements';
//import moment from 'moment';

export default class ForecastCard extends React.Component {

    state = { }

    componentDidMount() {
        let time; // Format date
        let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var date = new Date(this.props.detail.dt * 1000); // Create a new date from passed date time
            var hours = date.getHours(); // Hours from timestamp
            var minutes = "0" + date.getMinutes(); // Minutes from timestamp
            let dayName = days[date.getDay()]; // Weekday name
            let day = date.getDate(); // Numerical date
            let year = date.getFullYear();
            let month = months[date.getMonth()]; // Month name
            time = hours + ':' + minutes.substr(-2);
        this.setState({
            dayName,
            month,
            day,
            year,
            time
        })
    }

    render() {
        return(
            <View style = {styles.card}>
                <Text style = {styles.notes}>{this.props.location}</Text>
                <Text>{this.state.dayName}</Text>
                <Text>{this.state.month} {this.state.day}, {this.state.year}</Text>

                <View style = {styles.container}>
                    <Image style = {styles.image} source = {{uri:"https://openweathermap.org/img/w/" + this.props.detail.weather[0].icon + ".png"}}/>
                    <Text style = {styles.time}>{this.state.time}</Text>
                </View>

                <View style = {styles.container2}>
                    <Text style = {styles.notes}>{this.props.detail.weather[0].description}</Text>
                    <Text style = {styles.notes}>{Math.round(this.props.detail.main.temp * 10) / 10}&#8451;</Text>
                </View>

                <Divider style = {styles.divider}/>

            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    container2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    card: {
        backgroundColor: 'rgba(56, 172, 236, 1)',
        borderWidth: 0,
        borderRadius: 20,
    },
    notes: {
        fontSize: 18,
        color: '#fff',
        textTransform: 'capitalize'
    },
    time: {
        fontSize: 38,
        color: '#fff'
    },
    image: {
        width: 100,
        height: 100,
    },
    divider: {
        backgroundColor: '#dfe6e9',
        marginVertical: 20
    }
});
*/