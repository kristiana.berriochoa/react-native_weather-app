import React from 'react';

import { StyleSheet, Text } from 'react-native';


class FilterData extends React.Component {

    state={
        month: 1,
        date: 1,
        year: 2000 
    }

componentDidMount(props) {
    const datetime = this.props.datetime
    const datetimeSplit = datetime.split('')
    // console.log(datetimeSplit)
    const yearSplit =  datetimeSplit[0]+  datetimeSplit[1]+  datetimeSplit[2]+ datetimeSplit[3]
    const monthSplit =  datetimeSplit[5] + datetimeSplit[6]
    const dateSplit =  datetimeSplit[8]+ datetimeSplit[9]
    // console.log(yearSplit, monthSplit, dateSplit)
            this.setState({
                month: monthSplit,
                date: dateSplit,
                year: yearSplit,
            })
    this.props.catchCurrentDate(dateSplit, monthSplit, yearSplit)

}
render (){
    return (
         
                        <Text style={styles.header}>{this.state.date} {this.state.month} {this.state.year}</Text>
        
    )
}
}

const styles = StyleSheet.create({
    container: {
        display: 'flex'
    },
    header: {
        color: '#FFFFFF',
        marginTop: 30,
        fontSize: 25,
        textAlign: 'center',
        paddingRight: 20, 
        fontFamily: "sans-serif-medium"
    },
    image: {
      width: 32, 
      height: 40,
      marginLeft: 30, 
      marginTop: 40,
    }
    
  })

export default FilterData
